import React  from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const header = React.createElement(
  'h1',
  {style: { color: '#999', fontSize: '19px' }},
  'Solar system planets'
);

const listOfPlanet = (
  <ul className="planets-list">
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>
);
  
const slider = (
  <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox" />
    <div onClick={changeTheme} className="slider round"></div>
  </label>
);

function changeTheme(){
  document.body.classList.toggle('dark');
  const list = document.querySelector('ul');
  list.classList.toggle('darkModeForList');
}
ReactDOM.render(
  <>
    {header}
    {listOfPlanet}
    {slider}
  </>,
  document.getElementById('root')
);
